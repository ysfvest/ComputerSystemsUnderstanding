Integer Product combine1: Maximum use of data abstraction:
Best: 7.34 (2%), Overall Best: 7.70 40-most: 7.71 cycles/element
Integer Product combine2: Take vec_length() out of loop:
Best: 5.26 (2%), Overall Best: 5.42 40-most: 5.45 cycles/element
Integer Product combine3: Array reference to vector data:
Best: 6.60 (2%), Overall Best: 6.64 40-most: 6.80 cycles/element
Integer Product combine3w: Update *dest within loop only with write:
Best: 2.63 (18%), Overall Best: 2.63 40-most: 2.70 cycles/element
Integer Product combine4: Array reference, accumulate in temporary:
Best: 2.62 (4%), Overall Best: 2.62 40-most: 2.70 cycles/element
Integer Product combine4b: Include bonds check in loop:
Best: 2.62 (4%), Overall Best: 2.62 40-most: 2.70 cycles/element
Integer Product combine4p: Pointer reference, accumulate in temporary:
Best: 2.60 (2%), Overall Best: 2.62 40-most: 2.71 cycles/element
Integer Product combine5: Array code, unrolled by 2:
Best: 2.63 (2%), Overall Best: 2.63 40-most: 2.71 cycles/element
Integer Product combine5p: Pointer code, unrolled by 2, for loop:
Best: 2.60 (2%), Overall Best: 2.62 40-most: 2.71 cycles/element
Integer Product unroll2aw: Array code, unrolled by 2, while loop:
Best: 2.60 (2%), Overall Best: 2.60 40-most: 2.71 cycles/element
Integer Product unroll3a: Array code, unrolled by 3:
Best: 2.63 (6%), Overall Best: 2.63 40-most: 2.70 cycles/element
Integer Product unroll4a: Array code, unrolled by 4:
Best: 2.62 (6%), Overall Best: 2.62 40-most: 2.70 cycles/element
Integer Product unroll5a: Array code, unrolled by 5:
Best: 2.62 (4%), Overall Best: 2.63 40-most: 2.71 cycles/element
Integer Product unroll6a: Array code, unrolled by 6:
Best: 2.62 (8%), Overall Best: 2.64 40-most: 2.70 cycles/element
Integer Product unroll7a: Array code, unrolled by 7:
Best: 2.62 (4%), Overall Best: 2.63 40-most: 2.70 cycles/element
Integer Product unroll8a: Array code, unrolled by 8:
Best: 2.62 (6%), Overall Best: 2.70 40-most: 2.71 cycles/element
Integer Product unroll9a: Array code, unrolled by 9:
Best: 2.60 (2%), Overall Best: 2.65 40-most: 2.72 cycles/element
Integer Product unroll10a: Array code, unrolled by 10:
Best: 2.63 (2%), Overall Best: 2.67 40-most: 2.73 cycles/element
Integer Product unroll16a: Array code, unrolled by 16:
Best: 2.62 (4%), Overall Best: 2.62 40-most: 2.71 cycles/element
Integer Product unroll2: Pointer code, unrolled by 2:
Best: 2.61 (4%), Overall Best: 2.62 40-most: 2.71 cycles/element
Integer Product unroll3: Pointer code, unrolled by 3:
Best: 2.59 (2%), Overall Best: 2.68 40-most: 2.70 cycles/element
Integer Product unroll4: Pointer code, unrolled by 4:
Best: 2.62 (8%), Overall Best: 2.66 40-most: 2.70 cycles/element
Integer Product unroll8: Pointer code, unrolled by 8:
Best: 2.61 (2%), Overall Best: 2.68 40-most: 2.70 cycles/element
Integer Product unroll16: Pointer code, unrolled by 16:
Best: 2.61 (2%), Overall Best: 2.70 40-most: 2.71 cycles/element
Integer Product combine6: Array code, unrolled by 2, Superscalar x2:
Best: 1.31 (10%), Overall Best: 1.34 40-most: 1.36 cycles/element
Integer Product unroll4x2a: Array code, unrolled by 4, Superscalar x2:
Best: 1.30 (2%), Overall Best: 1.34 40-most: 1.35 cycles/element
Integer Product unroll8x2a: Array code, unrolled by 8, Superscalar x2:
Best: 1.30 (4%), Overall Best: 1.36 40-most: 1.36 cycles/element
Integer Product unroll3x3a: Array code, unrolled by 3, Superscalar x3:
Best: 0.85 (2%), Overall Best: 0.89 40-most: 0.90 cycles/element
Integer Product unroll4x4a: Array code, unrolled by 4, Superscalar x4:
Best: 0.87 (4%), Overall Best: 0.90 40-most: 0.90 cycles/element
Integer Product unroll5x5a: Array code, unrolled by 5, Superscalar x5:
Best: 0.87 (12%), Overall Best: 0.90 40-most: 0.90 cycles/element
Integer Product unroll6x6a: Array code, unrolled by 6, Superscalar x6:
Best: 0.87 (4%), Overall Best: 0.90 40-most: 0.90 cycles/element
Integer Product unroll7x7a: Array code, unrolled by 7, Superscalar x7:
Best: 0.87 (12%), Overall Best: 0.90 40-most: 0.90 cycles/element
Integer Product unroll8x4a: Array code, unrolled by 8, Superscalar x4:
Best: 0.87 (18%), Overall Best: 0.91 40-most: 0.91 cycles/element
Integer Product unroll8x8a: Array code, unrolled by 8, Superscalar x8:
Best: 0.86 (2%), Overall Best: 0.90 40-most: 0.90 cycles/element
Integer Product unroll9x9a: Array code, unrolled by 9, Superscalar x9:
Best: 0.87 (6%), Overall Best: 0.90 40-most: 0.90 cycles/element
Integer Product unroll10x10a: Array code, unrolled by 10, Superscalar x10:
Best: 0.87 (18%), Overall Best: 0.91 40-most: 0.90 cycles/element
Integer Product unroll2x6a: Array code, unrolled by 12, Superscalar x6:
Best: 0.86 (4%), Overall Best: 0.89 40-most: 0.90 cycles/element
Integer Product unroll12x12a: Array code, unrolled by 12, Superscalar x12:
Best: 0.86 (4%), Overall Best: 0.88 40-most: 0.90 cycles/element
Integer Product unroll16x16a: Array code, unrolled by 16, Superscalar x16:
Best: 0.86 (6%), Overall Best: 0.87 40-most: 0.90 cycles/element
Integer Product unroll20x20a: Array code, unrolled by 20, Superscalar x20:
Best: 0.86 (2%), Overall Best: 0.89 40-most: 0.91 cycles/element
Integer Product unroll8x2: Pointer code, unrolled by 8, Superscalar x2:
Best: 1.31 (24%), Overall Best: 1.32 40-most: 1.35 cycles/element
Integer Product unroll8x4: Pointer code, unrolled by 8, Superscalar x4:
Best: 0.87 (8%), Overall Best: 0.89 40-most: 0.90 cycles/element
Integer Product unroll8x8: Pointer code, unrolled by 8, Superscalar x8:
Best: 0.86 (2%), Overall Best: 0.88 40-most: 0.90 cycles/element
Integer Product unroll9x3: Pointer code, unrolled by 9, Superscalar x3:
Best: 0.86 (2%), Overall Best: 0.89 40-most: 0.91 cycles/element
Integer Product unrollx2as: Array code, Unroll x2, Superscalar x2, noninterleaved:
Best: 1.30 (2%), Overall Best: 1.32 40-most: 1.35 cycles/element
Integer Product combine7: Array code, unrolled by 2, different associativity:
Best: 1.30 (2%), Overall Best: 1.32 40-most: 1.35 cycles/element
Integer Product unroll3aa: Array code, unrolled by 3, Different Associativity:
Best: 0.87 (14%), Overall Best: 0.88 40-most: 0.90 cycles/element
Integer Product unroll4aa: Array code, unrolled by 4, Different Associativity:
Best: 0.86 (2%), Overall Best: 0.88 40-most: 0.90 cycles/element
Integer Product unroll5aa: Array code, unrolled by 5, Different Associativity:
Best: 0.86 (2%), Overall Best: 0.87 40-most: 0.90 cycles/element
Integer Product unroll6aa: Array code, unrolled by 6, Different Associativity:
Best: 0.87 (10%), Overall Best: 0.88 40-most: 0.89 cycles/element
Integer Product unroll7aa: Array code, unrolled by 7, Different Associativity:
Best: 0.87 (18%), Overall Best: 0.88 40-most: 0.89 cycles/element
Integer Product unroll8aa: Array code, unrolled by 8, Different Associativity:
Best: 0.85 (2%), Overall Best: 0.88 40-most: 0.90 cycles/element
Integer Product unroll9aa: Array code, unrolled by 9, Different Associativity:
Best: 0.87 (14%), Overall Best: 0.88 40-most: 0.90 cycles/element
Integer Product unroll10aa: Array code, unrolled by 10, Different Associativity:
Best: 0.86 (2%), Overall Best: 0.88 40-most: 0.90 cycles/element
Integer Product unroll12aa: Array code, unrolled by 12, Different Associativity:
Best: 0.86 (2%), Overall Best: 0.87 40-most: 0.90 cycles/element
Integer Product simd_v1: SSE code, 1*VSIZE-way parallelism:
Best: 1.10 (4%), Overall Best: 1.16 40-most: 1.16 cycles/element
Integer Product simd_v2: SSE code, 2*VSIZE-way parallelism:
Best: 0.53 (4%), Overall Best: 0.54 40-most: 0.56 cycles/element
Integer Product simd_v4: SSE code, 4*VSIZE-way parallelism:
Best: 0.28 (46%), Overall Best: 0.28 40-most: 0.29 cycles/element
Integer Product simd_v8: SSE code, 8*VSIZE-way parallelism:
Best: 0.11 (2%), Overall Best: 0.14 40-most: 0.14 cycles/element
Integer Product simd_v10: SSE code, 10*VSIZE-way parallelism:
Best: 0.11 (2%), Overall Best: 0.13 40-most: 0.13 cycles/element
Integer Product simd_v12: SSE code, 12*VSIZE-way parallelism:
Best: 0.11 (4%), Overall Best: 0.13 40-most: 0.14 cycles/element
Integer Product simd_v2a: SSE code, 2*VSIZE-way parallelism, reassociate:
Best: 0.53 (2%), Overall Best: 0.55 40-most: 0.56 cycles/element
Integer Product simd_v4a: SSE code, 4*VSIZE-way parallelism, reassociate:
Best: 0.25 (2%), Overall Best: 0.27 40-most: 0.27 cycles/element
Integer Product simd_v8a: SSE code, 8*VSIZE-way parallelism, reassociate:
Best: 0.07 (18%), Overall Best: 0.07 40-most: 0.10 cycles/element
