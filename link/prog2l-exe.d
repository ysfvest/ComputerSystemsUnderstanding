
prog2l:     file format elf64-x86-64
prog2l
architecture: i386:x86-64, flags 0x00000112:
EXEC_P, HAS_SYMS, D_PAGED
start address 0x0000000000400610

Program Header:
    PHDR off    0x0000000000000040 vaddr 0x0000000000400040 paddr 0x0000000000400040 align 2**3
         filesz 0x00000000000001f8 memsz 0x00000000000001f8 flags r-x
  INTERP off    0x0000000000000238 vaddr 0x0000000000400238 paddr 0x0000000000400238 align 2**0
         filesz 0x000000000000001c memsz 0x000000000000001c flags r--
    LOAD off    0x0000000000000000 vaddr 0x0000000000400000 paddr 0x0000000000400000 align 2**21
         filesz 0x0000000000000904 memsz 0x0000000000000904 flags r-x
    LOAD off    0x0000000000000e00 vaddr 0x0000000000600e00 paddr 0x0000000000600e00 align 2**21
         filesz 0x0000000000000258 memsz 0x0000000000000268 flags rw-
 DYNAMIC off    0x0000000000000e18 vaddr 0x0000000000600e18 paddr 0x0000000000600e18 align 2**3
         filesz 0x00000000000001e0 memsz 0x00000000000001e0 flags rw-
    NOTE off    0x0000000000000254 vaddr 0x0000000000400254 paddr 0x0000000000400254 align 2**2
         filesz 0x0000000000000044 memsz 0x0000000000000044 flags r--
EH_FRAME off    0x00000000000007e4 vaddr 0x00000000004007e4 paddr 0x00000000004007e4 align 2**2
         filesz 0x0000000000000034 memsz 0x0000000000000034 flags r--
   STACK off    0x0000000000000000 vaddr 0x0000000000000000 paddr 0x0000000000000000 align 2**4
         filesz 0x0000000000000000 memsz 0x0000000000000000 flags rw-
   RELRO off    0x0000000000000e00 vaddr 0x0000000000600e00 paddr 0x0000000000600e00 align 2**0
         filesz 0x0000000000000200 memsz 0x0000000000000200 flags r--

Dynamic Section:
  NEEDED               ./libvector.so
  NEEDED               libc.so.6
  INIT                 0x00000000004005a0
  FINI                 0x00000000004007c4
  INIT_ARRAY           0x0000000000600e00
  INIT_ARRAYSZ         0x0000000000000008
  FINI_ARRAY           0x0000000000600e08
  FINI_ARRAYSZ         0x0000000000000008
  GNU_HASH             0x0000000000400298
  STRTAB               0x0000000000400408
  SYMTAB               0x00000000004002d0
  STRSZ                0x00000000000000d5
  SYMENT               0x0000000000000018
  DEBUG                0x0000000000000000
  PLTGOT               0x0000000000601000
  PLTRELSZ             0x0000000000000060
  PLTREL               0x0000000000000007
  JMPREL               0x0000000000400540
  RELA                 0x0000000000400528
  RELASZ               0x0000000000000018
  RELAENT              0x0000000000000018
  VERNEED              0x00000000004004f8
  VERNEEDNUM           0x0000000000000001
  VERSYM               0x00000000004004de

Version References:
  required from libc.so.6:
    0x09691974 0x00 03 GLIBC_2.3.4
    0x09691a75 0x00 02 GLIBC_2.2.5

Sections:
Idx Name          Size      VMA               LMA               File off  Algn
  0 .interp       0000001c  0000000000400238  0000000000400238  00000238  2**0
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  1 .note.ABI-tag 00000020  0000000000400254  0000000000400254  00000254  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  2 .note.gnu.build-id 00000024  0000000000400274  0000000000400274  00000274  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  3 .gnu.hash     00000038  0000000000400298  0000000000400298  00000298  2**3
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  4 .dynsym       00000138  00000000004002d0  00000000004002d0  000002d0  2**3
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  5 .dynstr       000000d5  0000000000400408  0000000000400408  00000408  2**0
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  6 .gnu.version  0000001a  00000000004004de  00000000004004de  000004de  2**1
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  7 .gnu.version_r 00000030  00000000004004f8  00000000004004f8  000004f8  2**3
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  8 .rela.dyn     00000018  0000000000400528  0000000000400528  00000528  2**3
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  9 .rela.plt     00000060  0000000000400540  0000000000400540  00000540  2**3
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
 10 .init         0000001a  00000000004005a0  00000000004005a0  000005a0  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, CODE
 11 .plt          00000050  00000000004005c0  00000000004005c0  000005c0  2**4
                  CONTENTS, ALLOC, LOAD, READONLY, CODE
 12 .text         000001b2  0000000000400610  0000000000400610  00000610  2**4
                  CONTENTS, ALLOC, LOAD, READONLY, CODE
 13 .fini         00000009  00000000004007c4  00000000004007c4  000007c4  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, CODE
 14 .rodata       00000011  00000000004007d0  00000000004007d0  000007d0  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
 15 .eh_frame_hdr 00000034  00000000004007e4  00000000004007e4  000007e4  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
 16 .eh_frame     000000ec  0000000000400818  0000000000400818  00000818  2**3
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
 17 .init_array   00000008  0000000000600e00  0000000000600e00  00000e00  2**3
                  CONTENTS, ALLOC, LOAD, DATA
 18 .fini_array   00000008  0000000000600e08  0000000000600e08  00000e08  2**3
                  CONTENTS, ALLOC, LOAD, DATA
 19 .jcr          00000008  0000000000600e10  0000000000600e10  00000e10  2**3
                  CONTENTS, ALLOC, LOAD, DATA
 20 .dynamic      000001e0  0000000000600e18  0000000000600e18  00000e18  2**3
                  CONTENTS, ALLOC, LOAD, DATA
 21 .got          00000008  0000000000600ff8  0000000000600ff8  00000ff8  2**3
                  CONTENTS, ALLOC, LOAD, DATA
 22 .got.plt      00000038  0000000000601000  0000000000601000  00001000  2**3
                  CONTENTS, ALLOC, LOAD, DATA
 23 .data         00000020  0000000000601038  0000000000601038  00001038  2**3
                  CONTENTS, ALLOC, LOAD, DATA
 24 .bss          00000010  0000000000601058  0000000000601058  00001058  2**2
                  ALLOC
 25 .comment      00000056  0000000000000000  0000000000000000  00001058  2**0
                  CONTENTS, READONLY
SYMBOL TABLE:
0000000000400238 l    d  .interp	0000000000000000              .interp
0000000000400254 l    d  .note.ABI-tag	0000000000000000              .note.ABI-tag
0000000000400274 l    d  .note.gnu.build-id	0000000000000000              .note.gnu.build-id
0000000000400298 l    d  .gnu.hash	0000000000000000              .gnu.hash
00000000004002d0 l    d  .dynsym	0000000000000000              .dynsym
0000000000400408 l    d  .dynstr	0000000000000000              .dynstr
00000000004004de l    d  .gnu.version	0000000000000000              .gnu.version
00000000004004f8 l    d  .gnu.version_r	0000000000000000              .gnu.version_r
0000000000400528 l    d  .rela.dyn	0000000000000000              .rela.dyn
0000000000400540 l    d  .rela.plt	0000000000000000              .rela.plt
00000000004005a0 l    d  .init	0000000000000000              .init
00000000004005c0 l    d  .plt	0000000000000000              .plt
0000000000400610 l    d  .text	0000000000000000              .text
00000000004007c4 l    d  .fini	0000000000000000              .fini
00000000004007d0 l    d  .rodata	0000000000000000              .rodata
00000000004007e4 l    d  .eh_frame_hdr	0000000000000000              .eh_frame_hdr
0000000000400818 l    d  .eh_frame	0000000000000000              .eh_frame
0000000000600e00 l    d  .init_array	0000000000000000              .init_array
0000000000600e08 l    d  .fini_array	0000000000000000              .fini_array
0000000000600e10 l    d  .jcr	0000000000000000              .jcr
0000000000600e18 l    d  .dynamic	0000000000000000              .dynamic
0000000000600ff8 l    d  .got	0000000000000000              .got
0000000000601000 l    d  .got.plt	0000000000000000              .got.plt
0000000000601038 l    d  .data	0000000000000000              .data
0000000000601058 l    d  .bss	0000000000000000              .bss
0000000000000000 l    d  .comment	0000000000000000              .comment
0000000000000000 l    df *ABS*	0000000000000000              crtstuff.c
0000000000600e10 l     O .jcr	0000000000000000              __JCR_LIST__
0000000000400640 l     F .text	0000000000000000              deregister_tm_clones
0000000000400670 l     F .text	0000000000000000              register_tm_clones
00000000004006b0 l     F .text	0000000000000000              __do_global_dtors_aux
0000000000601058 l     O .bss	0000000000000001              completed.6982
0000000000600e08 l     O .fini_array	0000000000000000              __do_global_dtors_aux_fini_array_entry
00000000004006d0 l     F .text	0000000000000000              frame_dummy
0000000000600e00 l     O .init_array	0000000000000000              __frame_dummy_init_array_entry
0000000000000000 l    df *ABS*	0000000000000000              main2.c
0000000000000000 l    df *ABS*	0000000000000000              crtstuff.c
0000000000400900 l     O .eh_frame	0000000000000000              __FRAME_END__
0000000000600e10 l     O .jcr	0000000000000000              __JCR_END__
0000000000000000 l    df *ABS*	0000000000000000              
0000000000600e08 l       .init_array	0000000000000000              __init_array_end
0000000000600e18 l     O .dynamic	0000000000000000              _DYNAMIC
0000000000600e00 l       .init_array	0000000000000000              __init_array_start
0000000000601000 l     O .got.plt	0000000000000000              _GLOBAL_OFFSET_TABLE_
00000000004007c0 g     F .text	0000000000000002              __libc_csu_fini
0000000000000000  w      *UND*	0000000000000000              _ITM_deregisterTMCloneTable
0000000000601038  w      .data	0000000000000000              data_start
0000000000601058 g       .data	0000000000000000              _edata
000000000060105c g     O .bss	0000000000000008              z
0000000000601050 g     O .data	0000000000000008              x
00000000004007c4 g     F .fini	0000000000000000              _fini
0000000000000000       F *UND*	0000000000000000              __libc_start_main@@GLIBC_2.2.5
0000000000000000       F *UND*	0000000000000000              addvec
0000000000601038 g       .data	0000000000000000              __data_start
0000000000000000  w      *UND*	0000000000000000              __gmon_start__
0000000000601040 g     O .data	0000000000000000              .hidden __dso_handle
00000000004007d0 g     O .rodata	0000000000000004              _IO_stdin_used
0000000000400750 g     F .text	0000000000000065              __libc_csu_init
0000000000601068 g       .bss	0000000000000000              _end
0000000000400610 g     F .text	0000000000000000              _start
0000000000601048 g     O .data	0000000000000008              y
0000000000601058 g       .bss	0000000000000000              __bss_start
00000000004006fd g     F .text	0000000000000047              main
0000000000000000       F *UND*	0000000000000000              __printf_chk@@GLIBC_2.3.4
0000000000000000  w      *UND*	0000000000000000              _Jv_RegisterClasses
0000000000601058 g     O .data	0000000000000000              .hidden __TMC_END__
0000000000000000  w      *UND*	0000000000000000              _ITM_registerTMCloneTable
00000000004005a0 g     F .init	0000000000000000              _init



Disassembly of section .init:

00000000004005a0 <_init>:
  4005a0:	48 83 ec 08          	sub    $0x8,%rsp
  4005a4:	48 8b 05 4d 0a 20 00 	mov    0x200a4d(%rip),%rax        # 600ff8 <_DYNAMIC+0x1e0>
  4005ab:	48 85 c0             	test   %rax,%rax
  4005ae:	74 05                	je     4005b5 <_init+0x15>
  4005b0:	e8 3b 00 00 00       	callq  4005f0 <__gmon_start__@plt>
  4005b5:	48 83 c4 08          	add    $0x8,%rsp
  4005b9:	c3                   	retq   

Disassembly of section .plt:

00000000004005c0 <__libc_start_main@plt-0x10>:
  4005c0:	ff 35 42 0a 20 00    	pushq  0x200a42(%rip)        # 601008 <_GLOBAL_OFFSET_TABLE_+0x8>
  4005c6:	ff 25 44 0a 20 00    	jmpq   *0x200a44(%rip)        # 601010 <_GLOBAL_OFFSET_TABLE_+0x10>
  4005cc:	0f 1f 40 00          	nopl   0x0(%rax)

00000000004005d0 <__libc_start_main@plt>:
  4005d0:	ff 25 42 0a 20 00    	jmpq   *0x200a42(%rip)        # 601018 <_GLOBAL_OFFSET_TABLE_+0x18>
  4005d6:	68 00 00 00 00       	pushq  $0x0
  4005db:	e9 e0 ff ff ff       	jmpq   4005c0 <_init+0x20>

00000000004005e0 <addvec@plt>:
  4005e0:	ff 25 3a 0a 20 00    	jmpq   *0x200a3a(%rip)        # 601020 <_GLOBAL_OFFSET_TABLE_+0x20>
  4005e6:	68 01 00 00 00       	pushq  $0x1
  4005eb:	e9 d0 ff ff ff       	jmpq   4005c0 <_init+0x20>

00000000004005f0 <__gmon_start__@plt>:
  4005f0:	ff 25 32 0a 20 00    	jmpq   *0x200a32(%rip)        # 601028 <_GLOBAL_OFFSET_TABLE_+0x28>
  4005f6:	68 02 00 00 00       	pushq  $0x2
  4005fb:	e9 c0 ff ff ff       	jmpq   4005c0 <_init+0x20>

0000000000400600 <__printf_chk@plt>:
  400600:	ff 25 2a 0a 20 00    	jmpq   *0x200a2a(%rip)        # 601030 <_GLOBAL_OFFSET_TABLE_+0x30>
  400606:	68 03 00 00 00       	pushq  $0x3
  40060b:	e9 b0 ff ff ff       	jmpq   4005c0 <_init+0x20>

Disassembly of section .text:

0000000000400610 <_start>:
  400610:	31 ed                	xor    %ebp,%ebp
  400612:	49 89 d1             	mov    %rdx,%r9
  400615:	5e                   	pop    %rsi
  400616:	48 89 e2             	mov    %rsp,%rdx
  400619:	48 83 e4 f0          	and    $0xfffffffffffffff0,%rsp
  40061d:	50                   	push   %rax
  40061e:	54                   	push   %rsp
  40061f:	49 c7 c0 c0 07 40 00 	mov    $0x4007c0,%r8
  400626:	48 c7 c1 50 07 40 00 	mov    $0x400750,%rcx
  40062d:	48 c7 c7 fd 06 40 00 	mov    $0x4006fd,%rdi
  400634:	e8 97 ff ff ff       	callq  4005d0 <__libc_start_main@plt>
  400639:	f4                   	hlt    
  40063a:	66 0f 1f 44 00 00    	nopw   0x0(%rax,%rax,1)

0000000000400640 <deregister_tm_clones>:
  400640:	b8 5f 10 60 00       	mov    $0x60105f,%eax
  400645:	55                   	push   %rbp
  400646:	48 2d 58 10 60 00    	sub    $0x601058,%rax
  40064c:	48 83 f8 0e          	cmp    $0xe,%rax
  400650:	48 89 e5             	mov    %rsp,%rbp
  400653:	77 02                	ja     400657 <deregister_tm_clones+0x17>
  400655:	5d                   	pop    %rbp
  400656:	c3                   	retq   
  400657:	b8 00 00 00 00       	mov    $0x0,%eax
  40065c:	48 85 c0             	test   %rax,%rax
  40065f:	74 f4                	je     400655 <deregister_tm_clones+0x15>
  400661:	5d                   	pop    %rbp
  400662:	bf 58 10 60 00       	mov    $0x601058,%edi
  400667:	ff e0                	jmpq   *%rax
  400669:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)

0000000000400670 <register_tm_clones>:
  400670:	b8 58 10 60 00       	mov    $0x601058,%eax
  400675:	55                   	push   %rbp
  400676:	48 2d 58 10 60 00    	sub    $0x601058,%rax
  40067c:	48 c1 f8 03          	sar    $0x3,%rax
  400680:	48 89 e5             	mov    %rsp,%rbp
  400683:	48 89 c2             	mov    %rax,%rdx
  400686:	48 c1 ea 3f          	shr    $0x3f,%rdx
  40068a:	48 01 d0             	add    %rdx,%rax
  40068d:	48 d1 f8             	sar    %rax
  400690:	75 02                	jne    400694 <register_tm_clones+0x24>
  400692:	5d                   	pop    %rbp
  400693:	c3                   	retq   
  400694:	ba 00 00 00 00       	mov    $0x0,%edx
  400699:	48 85 d2             	test   %rdx,%rdx
  40069c:	74 f4                	je     400692 <register_tm_clones+0x22>
  40069e:	5d                   	pop    %rbp
  40069f:	48 89 c6             	mov    %rax,%rsi
  4006a2:	bf 58 10 60 00       	mov    $0x601058,%edi
  4006a7:	ff e2                	jmpq   *%rdx
  4006a9:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)

00000000004006b0 <__do_global_dtors_aux>:
  4006b0:	80 3d a1 09 20 00 00 	cmpb   $0x0,0x2009a1(%rip)        # 601058 <__TMC_END__>
  4006b7:	75 11                	jne    4006ca <__do_global_dtors_aux+0x1a>
  4006b9:	55                   	push   %rbp
  4006ba:	48 89 e5             	mov    %rsp,%rbp
  4006bd:	e8 7e ff ff ff       	callq  400640 <deregister_tm_clones>
  4006c2:	5d                   	pop    %rbp
  4006c3:	c6 05 8e 09 20 00 01 	movb   $0x1,0x20098e(%rip)        # 601058 <__TMC_END__>
  4006ca:	f3 c3                	repz retq 
  4006cc:	0f 1f 40 00          	nopl   0x0(%rax)

00000000004006d0 <frame_dummy>:
  4006d0:	48 83 3d 38 07 20 00 	cmpq   $0x0,0x200738(%rip)        # 600e10 <__JCR_END__>
  4006d7:	00 
  4006d8:	74 1e                	je     4006f8 <frame_dummy+0x28>
  4006da:	b8 00 00 00 00       	mov    $0x0,%eax
  4006df:	48 85 c0             	test   %rax,%rax
  4006e2:	74 14                	je     4006f8 <frame_dummy+0x28>
  4006e4:	55                   	push   %rbp
  4006e5:	bf 10 0e 60 00       	mov    $0x600e10,%edi
  4006ea:	48 89 e5             	mov    %rsp,%rbp
  4006ed:	ff d0                	callq  *%rax
  4006ef:	5d                   	pop    %rbp
  4006f0:	e9 7b ff ff ff       	jmpq   400670 <register_tm_clones>
  4006f5:	0f 1f 00             	nopl   (%rax)
  4006f8:	e9 73 ff ff ff       	jmpq   400670 <register_tm_clones>

00000000004006fd <main>:
  4006fd:	48 83 ec 08          	sub    $0x8,%rsp
  400701:	b9 02 00 00 00       	mov    $0x2,%ecx
  400706:	ba 5c 10 60 00       	mov    $0x60105c,%edx
  40070b:	be 48 10 60 00       	mov    $0x601048,%esi
  400710:	bf 50 10 60 00       	mov    $0x601050,%edi
  400715:	e8 c6 fe ff ff       	callq  4005e0 <addvec@plt>
  40071a:	8b 0d 40 09 20 00    	mov    0x200940(%rip),%ecx        # 601060 <z+0x4>
  400720:	8b 15 36 09 20 00    	mov    0x200936(%rip),%edx        # 60105c <z>
  400726:	be d4 07 40 00       	mov    $0x4007d4,%esi
  40072b:	bf 01 00 00 00       	mov    $0x1,%edi
  400730:	b8 00 00 00 00       	mov    $0x0,%eax
  400735:	e8 c6 fe ff ff       	callq  400600 <__printf_chk@plt>
  40073a:	b8 00 00 00 00       	mov    $0x0,%eax
  40073f:	48 83 c4 08          	add    $0x8,%rsp
  400743:	c3                   	retq   
  400744:	66 2e 0f 1f 84 00 00 	nopw   %cs:0x0(%rax,%rax,1)
  40074b:	00 00 00 
  40074e:	66 90                	xchg   %ax,%ax

0000000000400750 <__libc_csu_init>:
  400750:	41 57                	push   %r15
  400752:	41 89 ff             	mov    %edi,%r15d
  400755:	41 56                	push   %r14
  400757:	49 89 f6             	mov    %rsi,%r14
  40075a:	41 55                	push   %r13
  40075c:	49 89 d5             	mov    %rdx,%r13
  40075f:	41 54                	push   %r12
  400761:	4c 8d 25 98 06 20 00 	lea    0x200698(%rip),%r12        # 600e00 <__frame_dummy_init_array_entry>
  400768:	55                   	push   %rbp
  400769:	48 8d 2d 98 06 20 00 	lea    0x200698(%rip),%rbp        # 600e08 <__init_array_end>
  400770:	53                   	push   %rbx
  400771:	4c 29 e5             	sub    %r12,%rbp
  400774:	31 db                	xor    %ebx,%ebx
  400776:	48 c1 fd 03          	sar    $0x3,%rbp
  40077a:	48 83 ec 08          	sub    $0x8,%rsp
  40077e:	e8 1d fe ff ff       	callq  4005a0 <_init>
  400783:	48 85 ed             	test   %rbp,%rbp
  400786:	74 1e                	je     4007a6 <__libc_csu_init+0x56>
  400788:	0f 1f 84 00 00 00 00 	nopl   0x0(%rax,%rax,1)
  40078f:	00 
  400790:	4c 89 ea             	mov    %r13,%rdx
  400793:	4c 89 f6             	mov    %r14,%rsi
  400796:	44 89 ff             	mov    %r15d,%edi
  400799:	41 ff 14 dc          	callq  *(%r12,%rbx,8)
  40079d:	48 83 c3 01          	add    $0x1,%rbx
  4007a1:	48 39 eb             	cmp    %rbp,%rbx
  4007a4:	75 ea                	jne    400790 <__libc_csu_init+0x40>
  4007a6:	48 83 c4 08          	add    $0x8,%rsp
  4007aa:	5b                   	pop    %rbx
  4007ab:	5d                   	pop    %rbp
  4007ac:	41 5c                	pop    %r12
  4007ae:	41 5d                	pop    %r13
  4007b0:	41 5e                	pop    %r14
  4007b2:	41 5f                	pop    %r15
  4007b4:	c3                   	retq   
  4007b5:	66 66 2e 0f 1f 84 00 	data32 nopw %cs:0x0(%rax,%rax,1)
  4007bc:	00 00 00 00 

00000000004007c0 <__libc_csu_fini>:
  4007c0:	f3 c3                	repz retq 

Disassembly of section .fini:

00000000004007c4 <_fini>:
  4007c4:	48 83 ec 08          	sub    $0x8,%rsp
  4007c8:	48 83 c4 08          	add    $0x8,%rsp
  4007cc:	c3                   	retq   
