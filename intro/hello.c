/* $begin hello */
#include <stdio.h>
#include <stdint.h>

int main() 
{
    uint32_t c1 = 2147483647u;
    int32_t c2 = 2147483647;
    
    uint8_t b1 = (2147483647u > -2147483647 -1);
    uint8_t b2 = (c1 > c2 - 1);

    printf("b1 = %d, b2 = %d \n", b1, b2);
    printf("hello, world\n");
    return 0;
}
/* $end hello */

