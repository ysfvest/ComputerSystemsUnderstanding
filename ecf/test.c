#include <stdio.h>
#include <stdlib.h>
#include "csapp.h"

#define TYPE 5
#if TYPE == 0 /************************************************************/

/* 
 * test - tests the mysystem-ans program
 *   if called with no args, intentionally segfault
 *   otherwise exit status is argv[1]
 */
int main(int argc, char **argv) {

    printf("hello from test (%d)\n", atoi(argv[1]));
    if (argc == 1)
	*(char *)main = 0; /* seg fault */
    else 
	exit(atoi(argv[1]));

    exit(0); /* keeps gcc -W happy */
}

#elif TYPE==1 /************************************************************/

#include <stdio.h>
#include "csapp.h"

int main(int argc, char* argv[], char* env[]) {
  if (execve("/bin/ls", argv, env) == -1) {
    fprintf(stderr, "execve error: %s\n", strerror(errno));
    exit(1);
  }
}

#elif TYPE==2 /************************************************************/

int mysystem(char* command) {
  pid_t pid;
  int status;

  if ((pid = Fork()) == 0) {
    /* child process */
    char* argv[4] = { "", "-c", command, NULL };
    execve("/bin/sh", argv, environ);
  }

  /* print child pid so we can kill it */
  printf("child pid: %d\n", pid);

  if (Waitpid(pid, &status, 0) > 0) {
    /* exit normally */
    if (WIFEXITED(status))
      return WEXITSTATUS(status);

    /* exit by signal */
    if (WIFSIGNALED(status))
      return WTERMSIG(status);
  }
}

int main(int argc, char* argv[]) {
  int code;

  code = mysystem("exit 1");
  printf("normally exit, code: %d\n", code); fflush(stdout);

  code = mysystem("wait-sig");
  printf("exit caused by signal, code: %d\n", code); fflush(stdout);
  return 0;
}

#elif TYPE == 3

int mysystem(char* command)
{
	char* argv[4] = { "", "-c", command, NULL };
	return execve("/bin/sh", argv, environ);
}

int main(int argc, char** argv, char** envp)
{
	int re = mysystem(argv[1]);
	printf("return: %d./n", re);
	return re;
}

#elif TYPE == 4

/*
 * 8.25.c
 */
#include <stdio.h>
#include "csapp.h"

sigjmp_buf buf;

void handler(int sig) {
  /* jump */
  siglongjmp(buf, 1);
}

char* tfgets(char* s, int size, FILE* stream) {
  char* result;

  if (signal(SIGALRM, handler) == SIG_ERR)
    unix_error("set alarm handler error");
  else
    alarm(5);

  if (!sigsetjmp(buf, 1)) {
    return fgets(s, size, stream);
  } else {
    /* run out of time */
    return NULL;
  }
}

#define LEN 100

int main(int argc, char* argv[]) {
  char buf[LEN];
  char* input = tfgets(buf, LEN, stdin);

  if (input == NULL) {
    printf("nothing input: NULL\n");
  } else {
    printf("%s", input);
  }
  return 0;
}

#endif /************************************************************/